import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Order } from '../shared/order';
import { OrderService } from '../shared/order.service';
import { Observable } from 'rxjs';

@Injectable()
export class OrderResolver implements Resolve<any>{
  constructor (private orderService : OrderService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Order> {
    return this.orderService.getById(route.params['id']);
  }
}
