import { Component, Input } from '@angular/core';
import { ActivatedRoute} from '@angular/router';

@Component({
  templateUrl: './orderDetail.html',
  selector: 'app-order-detail'
})
export class DetailComponent {
  @Input() order;

  constructor(private route : ActivatedRoute) {
    route.data.subscribe(data => this.order = data['order']);
  }
}
