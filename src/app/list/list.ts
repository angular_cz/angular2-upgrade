import { Component, OnInit, NgZone } from '@angular/core';
import { OrderService } from '../shared/order.service';

@Component({
  templateUrl: './orderList.html',
  selector: 'app-order-list'
})
export class ListComponent implements OnInit {
  orders = [];

  constructor(private orderService: OrderService, private ngZone: NgZone) {

  }

  ngOnInit() {
    // workaround because of bug
    // TODO remove ASAP
    this.ngZone.run(() => {
      this.orderService.getList().subscribe((orders) => {
        this.orders = orders;
      });
    });
  }

}
