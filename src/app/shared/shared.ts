import * as angular from 'angular';
import { OrderService } from './order.service';
import { downgradeInjectable } from '@angular/upgrade/src/aot/downgrade_injectable';

angular.module('orderAdministration.shared', [])
  .service('orderService', downgradeInjectable(OrderService));
