export interface Address {
  zip: string;
  city : string;
  street: string;
}

export interface Order {
  id:number;
  name: string;
  email: string;
  address : Address;

}
