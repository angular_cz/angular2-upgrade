import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Order } from './order';

@Injectable()
export class OrderService{

  ordersUrl;

  constructor(private http: Http) {
    this.ordersUrl = environment.restUri + '/orders/';
  }

  getList() {
    return this.http.get(this.ordersUrl)
      .map((response=> response.json()))
  };

  getById(id) {
    return this.http.get(this.ordersUrl + id)
      .map((response=> response.json()))
  };

  create(order) {
    return this.http.post(this.ordersUrl, order)
      .toPromise();
  }

}
