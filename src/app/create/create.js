(function() {
  'use strict';

  function OrderCreateController(orderService, $location) {

    this.save = function(order) {
      orderService.create(order)
        .then(function() {
          $location.path('/orders');
        });
    }
  }

  var orderCreateComponent = {
    templateUrl: 'src/app/create/orderCreate.html',
    controller: OrderCreateController,
    controllerAs: 'create'
  };


  function configRouter($routeProvider) {

    $routeProvider
      .when('/create', {
        template: '<order-create></order-create>'
      })
  }

  angular.module('orderAdministration.create', ['ngMessages', 'ngRoute', 'orderAdministration.shared'])
    .component('orderCreate', orderCreateComponent)
    .config(configRouter);

})();
