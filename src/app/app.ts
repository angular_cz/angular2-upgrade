import * as angular from 'angular';
import './shared/shared.ts';

angular.module('orderAdministration', [
    'templates',
    'orderAdministration.create']);
