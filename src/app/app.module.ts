import './app.ts';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { UpgradeModule } from '@angular/upgrade/static';
import { HybridInfoComponent } from './info/info.component';
import { AppComponent } from './app.component';
import { ListComponent } from './list/list';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { DetailComponent } from './detail/detail';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    UpgradeModule,
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    HttpModule
  ],
  declarations: [
    HybridInfoComponent,
    AppComponent,
    ListComponent,
    DetailComponent],

  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule {
  ngDoBootstrap() {
  }
}


