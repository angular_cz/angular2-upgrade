import './detail/detail';
import { NgModule } from '@angular/core';
import { RouterModule, UrlHandlingStrategy, UrlTree } from '@angular/router';
import { ListComponent } from './list/list';
import { APP_BASE_HREF } from '@angular/common';
import { SharedModule } from './shared/shared.module';
import { DetailComponent } from './detail/detail';
import { OrderResolver } from './detail/order-resolver.service';

export class HybridUrlHandlingStrategy implements UrlHandlingStrategy {
  shouldProcessUrl(url: UrlTree) {
    const startsWith = [
      '/orders',
      '/detail'
    ];

    return startsWith.some(urlItem => url.toString().startsWith(urlItem))
      || url.toString() === '/';
  }

  extract(url: UrlTree) {
    return url;
  }

  merge(url: UrlTree, whole: UrlTree) {
    return url;
  }
}

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forRoot([
      {path: 'orders', component: ListComponent},
      {path: 'detail/:id', component: DetailComponent, resolve: {
        order : OrderResolver
      }},
      {path: '', redirectTo: 'orders', pathMatch: 'full'}
    ],{
      useHash: true,
      initialNavigation: false
    })
  ],

  exports: [
    RouterModule
  ],

  providers: [
    OrderResolver,
    {provide: APP_BASE_HREF, useValue: '!'},
    {provide: UrlHandlingStrategy, useClass: HybridUrlHandlingStrategy},
  ],
})
export class AppRoutingModule {
}


