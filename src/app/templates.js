'use strict';

// file automatically generated by angular-template-cache

angular
	.module('templates', [])
	.run(['$templateCache', function($templateCache) {
		$templateCache.put('src/app/create/orderCreate.html', '<h1>Nová objednávka pro uživatele</h1>\n' +
			'<form name="createForm" novalidate ng-submit="createForm.$valid && create.save(create.order)">\n' +
			'  <div class="row col-md-6">\n' +
			'    <p>\n' +
			'      Jméno:\n' +
			'      <input type="text" ng-model="create.order.name" name="name" class="form-control" required>\n' +
			'    </p>\n' +
			'    <div ng-messages="createForm.name.$error" ng-if="createForm.name.$touched || createForm.$submitted" class="validations">\n' +
			'      <div ng-message="required">Zadejte jméno</div>\n' +
			'    </div>\n' +
			'\n' +
			'    <p>\n' +
			'      Email:\n' +
			'      <input type="email" ng-model="create.order.email" name="email" class="form-control" required>\n' +
			'    </p>\n' +
			'    <div ng-messages="createForm.email.$error" ng-if="createForm.email.$touched || createForm.$submitted" class="validations">\n' +
			'      <div ng-message="required">Zadejte email</div>\n' +
			'      <div ng-message="email">Zadejte platný email</div>\n' +
			'    </div>\n' +
			'\n' +
			'    <button class="btn btn-success" type="submit">Uložit</button>\n' +
			'    <a class="btn btn-warning" ng-href="#!/orders">Zrušit</a>\n' +
			'  </div>\n' +
			'</form>');

		$templateCache.put('src/app/detail/orderDetail.html', '<h1>Objednávka - {{order.name}}</h1>\n' +
			'<span class="lead">{{order.email}}</span>\n' +
			'<h2>Adresa</h2>\n' +
			'<p class="lead">\n' +
			'  {{order.name}}, {{order.address.street}}<br>\n' +
			'  {{order.address.zip}} {{order.address.city}}\n' +
			'</p>\n' +
			'<app-info></app-info>\n' +
			'\n' +
			'<a class="back" routerlink="/orders">Zpět na seznam</a>');

		$templateCache.put('src/app/info/info.component.html', '<p class="well">\n' +
			'  Additional Information ...\n' +
			'</p>');

		$templateCache.put('src/app/list/orderList.html', '<div class="orders">\n' +
			'  <h3>Objednávky</h3>\n' +
			'\n' +
			'  <p class="text-right">\n' +
			'    <a class="btn btn-success" routerlink="/create">Vytvořit</a>\n' +
			'  </p>\n' +
			'\n' +
			'  <table class="table">\n' +
			'    <thead>\n' +
			'      <tr>\n' +
			'        <th>Id</th>\n' +
			'        <th>Zákazník</th>\n' +
			'        <th>Email</th>\n' +
			'        <th>Akce</th>\n' +
			'      </tr>\n' +
			'    </thead>\n' +
			'    <tbody>\n' +
			'      <tr *ngfor="let order of orders" class="status-{{order.status}}">\n' +
			'        <td> {{order.id}}</td>\n' +
			'        <td>{{order.name}}</td>\n' +
			'        <td>\n' +
			'          {{order.email}}\n' +
			'        </td>\n' +
			'        <td class="text-right">\n' +
			'          <a [routerlink]="[\'/detail\', order.id]" class="btn btn-info">Detail</a>\n' +
			'        </td>\n' +
			'      </tr>\n' +
			'    </tbody>\n' +
			'  </table>\n' +
			'</div>');
	}
]);
